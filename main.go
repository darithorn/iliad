package main

import (
    . "iliad"
    "bufio"
    "fmt"
    "os"
)

func PrintNodes(node *Node) {
    switch node.NType {
    case NODE_LITERAL, NODE_WORD:
        fmt.Printf("%s ", node.Value)
    case NODE_TYPE:
        fmt.Printf(":")
        switch node.TType {
        case TYPE_PTR:
            fmt.Printf("*%s ", node.Value)
        case TYPE_ARRAY:
            for i := 0; i < node.ArrayN; i++ {
                fmt.Printf("[]")
            }
            fmt.Printf("%s ", node.Value)
        case TYPE_NORM, TYPE_CUST:
            fmt.Printf("%s ", node.Value)
        case TYPE_FUNC:
            fmt.Print("(func (")
            for _, n := range node.Children {
                PrintNodes(n)
            }
            fmt.Print(") ")
            PrintNodes(node.RType)
            fmt.Print(") ")
        }
    case NODE_EXPR:
        fmt.Print("(")
        for _, n := range node.Children {
            PrintNodes(n)
        }
        fmt.Print(") ")
    }
}

func prettyPrintType(t *TypeNode) string {
    if (t == nil) {
        return ""
    }
    output := ""
    if t.IsFunction {
        output = fmt.Sprintf("func params: %d return: %s", len(t.ParameterTypes), prettyPrintType(t.ReturnType))
    } else {
        output = t.Name
    }
    if t.IsPointer {
        for i := 0; i < t.PointerCount; i++ {
            output = "*" + output
        }
    }
    if t.IsArray {
        for i := 0; i < t.ArrayCount; i++ {
            output = "[]" + output
        }
    }
    
    return output
}

func prettyPrintNode(n ParserNode) {
    for ; n != nil; n = n.GetNext() {
        switch n.GetNodeType() {
        case NodeDefVar:
            defVar := n.(*DefVarNode)
            fmt.Printf("var: name: %s value: ", defVar.Name)
            prettyPrintNode(defVar.Value)
            fmt.Printf("type: %s\n", prettyPrintType(defVar.Type))
        case NodeType:
            typeNode := n.(*TypeNode)
            fmt.Printf("type: %s\n", prettyPrintType(typeNode))
        case NodeLambda:
            lambdaNode := n.(*LambdaNode)
            fmt.Printf("lambda: args: %d return: %s\nbody:\n", len(lambdaNode.Arguments), prettyPrintType(lambdaNode.ReturnType))
            prettyPrintNode(lambdaNode.Body)
            fmt.Println("\nend body")
        case NodeLiteral:
            literalNode := n.(*LiteralNode)
            fmt.Printf("%s ", literalNode.Value)
        case NodeMacro:
            fmt.Print("macro: ") 
            fallthrough
        case NodeFunctionCall:
            funcNode := n.(*FunctionCallNode)
            fmt.Printf("func name: ")
            prettyPrintNode(funcNode.Function)
            fmt.Printf("args: ")
            prettyPrintNode(funcNode.Arguments)
        case NodeIf:
            ifNode := n.(*IfNode)
            fmt.Printf("if: ")
            prettyPrintNode(ifNode.BooleanExpr)
            fmt.Print("true: ")
            prettyPrintNode(ifNode.TrueExpr)
            fmt.Print("false: ")
            prettyPrintNode(ifNode.FalseExpr)
        case NodeStruct:
            structNode := n.(*StructNode)
            fmt.Printf("struct: %s fields: %d", structNode.Name, len(structNode.Fields))
        default:
            fmt.Println("Unknown node type")
        }
    }
}

func main() {
    file, err := os.Open("test.il")
    defer file.Close()
    if err != nil {
        panic(err) // Just panic for now.
    }
    reader := bufio.NewReader(file)
    lexer := NewLexer(reader)
    str, err := lexer.GetToken()
    if err != nil {
        fmt.Println(err)
    }
    node := Parse(str)
    prettyPrintNode(node)
}