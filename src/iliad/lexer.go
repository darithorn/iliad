package iliad

import (
    "bufio"
    "errors"
    "io"
    "unicode"
    "fmt"
)

const(
    NODE_LITERAL=iota
    NODE_WORD=iota
    NODE_EXPR=iota
    NODE_TYPE=iota
)

const(
    TYPE_NORM=iota // built-in type
    TYPE_ARRAY=iota // array type
    TYPE_FUNC=iota  // function
    TYPE_PTR=iota   // pointer
    TYPE_CUST=iota  // typedefined
)

// contains lexer state
type Lexer struct {
    reader *bufio.Reader
    cur rune
    prevCharPos int // last character position of previous line
    LineNum int
    CharPos int
}

func NewLexer(reader *bufio.Reader) *Lexer {
    return &Lexer { reader: reader, LineNum: 1, CharPos: 1 }
}

func (l *Lexer) NewError(format string, a ...interface{}) error {
    return errors.New(fmt.Sprintf("[L%d:C%d] %s\n", l.LineNum, l.CharPos, fmt.Sprintf(format, a ...)))
}

func (l *Lexer) advanceLine() {
    l.prevCharPos = l.CharPos
    l.CharPos = 1
    l.LineNum++
}

func (l *Lexer) ReadRune() (r rune, size int, err error) {
    r, size, err = l.reader.ReadRune()
    l.cur = r
    l.CharPos++
    if r == '\n' {
        l.advanceLine()
    }
    return r, size, err
}

func (l *Lexer) ReadLine() (line []byte, isPrefix bool, err error) {
    l.advanceLine()
    return l.reader.ReadLine()
}

func (l *Lexer) UnreadRune() error {
    l.CharPos--
    if l.cur == '\n' {
        l.CharPos = l.prevCharPos
        l.LineNum--
    }
    return l.reader.UnreadRune()
}

type Node struct {
    Value string
    NType int
    Parent *Node
    Children []*Node
    TType int   // the type of the type TYPE_
    // TYPE_ARRAY
    ArrayN int // How many consecutive [] braces for an array
    // TYPE_FUNC
    // Children are parameter types for function
    RType *Node // the return type of function
}

const(
    TokenWord = iota
    TokenString = iota
    TokenNumber = iota
    TokenOther = iota
    TokenEOF = iota
)
type Token struct {
    Value string
    TokType int
    next *Token // the next token
    LineNum int
    CharPos int
}
func (t *Token) Next() *Token {
    if t.next == nil {
        return &Token{Value: "EOF", TokType: TokenEOF, next: nil, LineNum: t.LineNum, CharPos: t.CharPos + 1}
    }
    return t.next
}
func (t *Token) Print() {
    tokType := ""
    switch t.TokType {
    case TokenWord:
        tokType = "word"
    case TokenString:
        tokType = "string"
    case TokenNumber:
        tokType = "number"
    case TokenOther:   
        tokType = "other"
    case TokenEOF:
        tokType = "eof"
    }
    fmt.Printf("tok: value: %s type: %s\n", t.Value, tokType)
}

func (n *Node) appendChild(child *Node) {
    child.Parent = n
    n.Children = append(n.Children, child)
}

func isDelimiter(c rune) bool {
    delimiters := " \n\t\r()"
    for _, s := range delimiters {
        if c == s {
            return true
        }
    }
    return false
}
/*
func (reader *Lexer) ReadType() (*Node, error) {
    result := ""
    TType := 0
    ArrayN := 0
    var RType *Node = nil
    var Children []*Node = nil
    var err error = nil
    c, _, err := reader.ReadRune()
    if c == '[' { // array type
        // :[]{name}
        TType = TYPE_ARRAY
        expect_close := true
        // grabs every consecutive [] and adds to ArrayN
        for c, _, err = reader.ReadRune(); err == nil; c, _, err = reader.ReadRune() {
            if expect_close {
                if c == ']' {
                    expect_close = false
                    ArrayN += 1
                } else {
                    return nil, reader.NewError("Expected closing ]")
                }
            } else if c == '[' {
                expect_close = true
            } else if unicode.IsLetter(c) {
                break
            }
        }
        reader.UnreadRune()
        result, err = reader.ReadWord()
    } else if c == '*' { // Pointer type
        // :*{name}
        TType = TYPE_PTR
        result, err = reader.ReadWord()
    } else if c == '(' { // function type
        // :(func ([type...]) {return_type})
        TType = TYPE_FUNC
        var l *Node
        l, err = reader.ReadList(false)
        for i, n := range l.Children {
            if i == 0 {
                if n.NType != NODE_WORD && n.Value != "func" {
                    return nil, reader.NewError("ReadType: Expected func after (")
                }
            } else if i == 1 {
                if n.NType != NODE_EXPR {
                    return nil, reader.NewError("ReadType: Expected argument list")
                }
                for _, nn := range n.Children {
                    if nn.NType != NODE_TYPE {
                        return nil, reader.NewError("ReadType: Expected type")
                    }
                }
            } else if i == 2 {
                if n.NType != NODE_TYPE {
                    return nil, reader.NewError("ReadType: Expected return type")
                }
            } else {
                return nil, reader.NewError("ReadType: Function type should only have 3 elements")
            }
        }
        Children = l.Children[1].Children
        RType = l.Children[2]
    } else { // must be a built-in or custom type
        // :{name}
        reader.UnreadRune()
        TType = TYPE_CUST
        result, err = reader.readWord()
        switch result {
        case "string", "int", "char", "bool", "void":
            TType = TYPE_NORM
        }
    }
    if err == io.EOF {
        err = nil
    }
    return &Node{Value: result, NType: NODE_TYPE, Children:Children, TType: TType, ArrayN: ArrayN, RType: RType}, err
}
*/

func (reader *Lexer) readWord() (*Token, error) {
    tok := &Token{Value: "", TokType: TokenWord}
    c, _, err := reader.ReadRune()
    for ; err == nil; c, _, err = reader.ReadRune() {
        if isDelimiter(c) {
            reader.UnreadRune()
            return tok, nil
        } else if unicode.IsLetter(c) {
            tok.Value += string(c)
        } else {
            return nil, reader.NewError("ReadWord: Expected letter")
        }
    }
    if err == io.EOF {
        err = nil
    }
    return tok, err
}

func (reader *Lexer) readNumber() (*Token, error) {
    tok := &Token{Value: "", TokType: TokenNumber}
    c, _, err := reader.ReadRune()
    decimal := false
    expectNext := false
    for ; err == nil; c, _, err = reader.ReadRune() {
        append := false // used to avoid code duplication. indicates whether to append current character
        if c == '.' {
            if decimal {
                return nil, reader.NewError("ReadNumber: Multiple decimals!")
            }
            decimal = true
            expectNext = true
            append = true
        } else if unicode.IsNumber(c) {
            expectNext = false
            append = true
        } else if isDelimiter(c) {
            if expectNext {
                return nil, reader.NewError("ReadNumber: Expected digit!")
            }
            reader.UnreadRune()
            return tok, nil
        } else {
            return nil, reader.NewError("ReadNumber: Expected digit!")
        }
        if append {
            tok.Value += string(c)
        }
    }
    if err == io.EOF {
        err = nil
    }
    return tok, err
}

func (reader *Lexer) readString() (*Token, error) {
    tok := &Token{Value: "", TokType: TokenString}
    escaped := false
    c, _, err := reader.ReadRune()
    for ; err == nil; c, _, err = reader.ReadRune() {
        if escaped {
            // For escape characters
            switch c {
            case 'a':
                tok.Value += "\a"
            case 'b':
                tok.Value += "\b"
            case 'f':
                tok.Value += "\f"
            case 'n':
                tok.Value += "\n"
            case 'r':
                tok.Value += "\r"
            case 't':
                tok.Value += "\t"
            case 'v':
                tok.Value += "\v"
            case '\\':
                tok.Value += "\\"
            case '\'':
                tok.Value += "'"
            case '"':
                tok.Value += "\""
            default:
                return nil, reader.NewError("ReadString: Unknown escape character")
            }
            escaped = false
        } else { // not escaped
            if c == '"' { 
                return tok, nil
            } else if c == '\\' { // must be escaped character
                escaped = true
            } else {
                tok.Value += string(c)
            }
        }  
    }
    if err == io.EOF {
        err = nil
    }
    return tok, err
}

func (reader *Lexer) GetToken() (*Token, error) {
    tok := &Token{}
    c, _, err := reader.ReadRune()
    for ; unicode.IsSpace(c); c, _, err = reader.ReadRune() {
        // Read runes until it isn't a space
    }
    if c == ';' {
        reader.ReadLine()
        c, _, err = reader.ReadRune()
        // This is to get passed \r\n
        for ; unicode.IsSpace(c); c, _, err = reader.ReadRune() {
            // Read runes until it isn't a space
        }
    }
    tok.LineNum = reader.LineNum
    tok.CharPos = reader.CharPos
    switch true {
    case unicode.IsNumber(c): // parse number
        reader.UnreadRune()
        tok, err = reader.readNumber()
        if err != nil {
            return nil, err
        }
    case c == '"': // parse string
        tok, err = reader.readString()
        if err != nil {
            return nil, err
        }
    case unicode.IsLetter(c): // Parse Word
        reader.UnreadRune()
        tok, err = reader.readWord()
        if err != nil {
            return nil, err
        }
    default:
        val := string(c)
        if c == '.' {
            c, _, err = reader.ReadRune()
            if c == '.' {
                c, _, err = reader.ReadRune()
                if c == '.' {
                    val = "..."
                } else {
                    reader.UnreadRune()
                    reader.UnreadRune()
                }
            } else {
                reader.UnreadRune()
            }
        }
        tok = &Token{Value: val, TokType: TokenOther}
    }
    if err == io.EOF {
        err = nil
        return nil, nil
    }
    nextToken, tokErr := reader.GetToken()
    if tokErr != nil {
        return nil, tokErr
    }
    tok.next = nextToken    
    return tok, err
}