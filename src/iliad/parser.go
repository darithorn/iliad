package iliad

import(
    "fmt"
)

type patternParam struct {
    name string
    ellipsis bool
}
type form struct {
    pattern []*Token
    ellipsis bool
}
type macro struct {
    name string
    pattern []*Token
    patternParams []*patternParam
    result ParserNode
}
func (m *macro) match(t *Token) bool {
    i := 0
    for tok := t; tok != nil; tok = tok.Next() {
        if tok.TokType == TokenOther && tok.Value != m.pattern[i].Value {
            // if it's a character like a parenthesis
            // the parenthesis is required for the pattern
            return false
        }
        i++
    }
    return true
}

type parser struct {
    macros []*macro
    errs []error
}

func (p *parser) newError(tok *Token, format string, a ...interface{}) {
    p.errs = append(p.errs, fmt.Errorf("[L%d:C%d] %s\n", tok.LineNum, tok.CharPos, fmt.Sprintf(format, a ...)))
}

/// Looks at tok's Next() and tests against given value
func expectValue(tok *Token, value string) bool {
    if tok == nil {
        return false
    }
    return tok.Value == value
}

func expectType(tok *Token, tokType int) bool {
    return tok.TokType == tokType
}

const(
    NodeDefVar = iota
    NodeType = iota
    NodeLambda = iota
    NodeLiteral = iota
    NodeFunctionCall = iota
    NodeIf = iota
    NodeStruct = iota
    NodeMacro = iota
    NodeError = iota
)

const(
    TypeSimple = iota // built-ins and structures
)

type ParserNode interface {
    GetNext() ParserNode
    setNext(p ParserNode)
    GetNodeType() int
}
type ErrorNode struct {
    Next ParserNode
}
func (e *ErrorNode) GetNext() ParserNode {
    return e.Next
}
func (e *ErrorNode) setNext(p ParserNode) {
    e.Next = p
}
func (e *ErrorNode) GetNodeType() int {
    return NodeError
}
type FunctionCallNode struct {
    Next ParserNode
    isMacro bool
    Function ParserNode // can be literal or lambda
    Arguments ParserNode
}
func (f *FunctionCallNode) GetNext() ParserNode {
    return f.Next
}
func (f *FunctionCallNode) setNext(p ParserNode) {
    f.Next = p
}
func (f *FunctionCallNode) GetNodeType() int {
    if f.isMacro {
        return NodeMacro
    }
    return NodeFunctionCall
}

type LiteralNode struct {
    Next ParserNode
    Value string
    LiteralType int
}
func (l *LiteralNode) GetNext() ParserNode {
    return l.Next
}
func (l *LiteralNode) setNext(p ParserNode) {
    l.Next = p
}
func (l *LiteralNode) GetNodeType() int {
    return NodeLiteral
}

type DefVarNode struct {
    Next ParserNode
    Name string
    Value ParserNode
    Type *TypeNode
}
func (d *DefVarNode) GetNext() ParserNode {
    return d.Next
}
func (d *DefVarNode) setNext(p ParserNode) {
    d.Next = p
}
func (d *DefVarNode) GetNodeType() int {
    return NodeDefVar
}

type argument struct {
    name string
    argType *TypeNode
}
type LambdaNode struct {
    Next ParserNode
    Arguments []argument
    ReturnType *TypeNode
    Body ParserNode
}
func (l *LambdaNode) GetNext() ParserNode {
    return l.Next
}
func (l *LambdaNode) setNext(p ParserNode) {
    l.Next = p
}
func (l *LambdaNode) GetNodeType() int {
    return NodeLambda
}

type TypeNode struct {
    Next ParserNode
    TType int
    // pointer
    PointerCount int
    IsPointer bool
    // array
    ArrayCount int
    IsArray bool
    // simple
    Name string
    // function
    IsFunction bool
    ParameterTypes []*TypeNode
    ReturnType *TypeNode
}
func (t *TypeNode) GetNext() ParserNode {
    return t.Next
}
func (t *TypeNode) setNext(p ParserNode) {
    t.Next = p
}
func (t *TypeNode) GetNodeType() int {
    return NodeType
}

type IfNode struct {
    Next ParserNode
    BooleanExpr ParserNode
    TrueExpr ParserNode
    FalseExpr ParserNode
}
func (i *IfNode) GetNext() ParserNode {
    return i.Next
}
func (i *IfNode) setNext(p ParserNode) {
    i.Next = p
} 
func (i *IfNode) GetNodeType() int {
    return NodeIf
}
type StructNode struct {
    Next ParserNode
    Name string
    Fields []argument
}
func (i *StructNode) GetNext() ParserNode {
    return i.Next
}
func (i *StructNode) setNext(p ParserNode) {
    i.Next = p
} 
func (i *StructNode) GetNodeType() int {
    return NodeStruct
}
/*
func (p *parser) parseMacroArgs(m macro, tok *Token) *Token {
    args := make([]ParserNode, 0, 3)
    notPattern := false                
    tok = tok.Next()
    for _, mp := range m.pattern {
        if expectType(mp, TokenOther) {
            if tok.Value != mp.Value {
                // TokenOther must have same value
                notPattern = true
                break
            }
        } else if expectType(tok, TokenOther) {
            // if token is TokenOther and pattern isn't
            // then it doesn't match
            notPattern = true
            break
        }
        var tmpNode ParserNode
        tmpNode, tok = p.parseToken(tok)
        openParens := 0
        for ; !expectType(tok, TokenEOF); tok = tok.Next() {
            if expectValue(tok, ")") {
                if openParens == 0 {
                    // the end of the macro definition
                    break
                }
                openParens--
            } else if expectValue(tok, "(") {
                openParens++
            }
            m.result = append(m.result, tok)
        }
        args = append(args, tmpNode)    
    }
    tok = tok.Next()    
    if notPattern {
        p.newError(tok, "not correct pattern for macro")
    }
    // Generate tokens
    return p.generateMacroTokens(m, args)
}

func (p *parser) generateMacroTokens(m macro, args []*Token) *Token {
    return nil
}
*/
func (p *parser) parseType(tok *Token) (*TypeNode, *Token) {
    // current tok is :
    tok = tok.Next()    
    node := &TypeNode{ }
    if expectValue(tok, "[") {
        node.IsArray = true
        for ; tok != nil && expectValue(tok, "["); tok = tok.Next() {
            // current tok is [
            tok = tok.Next()
            if !expectValue(tok, "]") {
                p.newError(tok, "expected ]")
            }
            node.ArrayCount++
        }
    }
    if expectValue(tok, "*") {
        node.IsPointer = true
        for ; !expectType(tok, TokenEOF) && expectValue(tok, "*"); tok = tok.Next() {
            node.PointerCount++
        }
    }
    if expectValue(tok, "(") {
        tok = tok.Next()
        if expectValue(tok, "func") {
            node.IsFunction = true
            tok = tok.Next()
            if !expectValue(tok, "(") {
                p.newError(tok, "expected (")
            }
            tok = tok.Next()
            for ; !expectType(tok, TokenEOF) && !expectValue(tok, ")"); {
                if expectValue(tok, ":") { // make sure it's the start of a type
                    var tmp *TypeNode
                    tmp, tok = p.parseType(tok)
                    node.ParameterTypes = append(node.ParameterTypes, tmp)
                } else {
                    p.newError(tok, "expected type")
                }  
                if expectValue(tok, ")") {
                    break
                }
            }
            if !expectValue(tok, ")") {
                p.newError(tok, "expected )")
            }
            tok = tok.Next()
            if !expectValue(tok, ":") {
                p.newError(tok, "expected type")
            }
            node.ReturnType, tok = p.parseType(tok)
            if !expectValue(tok, ")") {
                p.newError(tok, "expected )")
            }
            tok = tok.Next()
        }
    } else if expectType(tok, TokenWord) {
        // a simple type
        node.TType = TypeSimple
        node.Name = tok.Value
        tok = tok.Next()
    }
    return node, tok
}

func (p *parser) parseDefVar(tok *Token) (*DefVarNode, *Token) {
    // current tok is var
    tok = tok.Next() // now is a word
    if !expectType(tok, TokenWord) {
        p.newError(tok, "expected identifer got %s", tok.Value)
    }
    node := &DefVarNode{}
    node.Name = tok.Value
    tok = tok.Next() // either type or literal
    if expectValue(tok, ":") {
        // we're given a type
        node.Type, tok = p.parseType(tok)
    }
    node.Value, tok = p.parseToken(tok)
    if !expectValue(tok, ")") {
        p.newError(tok, "expected closing parenthesis")
    }
    tok = tok.Next()
    return node, tok
}

func (p *parser) parseLambda(tok *Token) (*LambdaNode, *Token) {
    node := &LambdaNode{}
    // lambda
    tok = tok.Next()
    if !expectValue(tok, "(") {
        p.newError(tok, "expected (")
    }
    tok = tok.Next()
    for ; tok != nil && tok.Value != ")"; {
        if expectType(tok, TokenWord) { // make sure it's a word
            arg := argument{}
            arg.name = tok.Value     
            tok = tok.Next()                
            if expectValue(tok, ":") { // make sure it's the start of a type
                arg.argType, tok = p.parseType(tok)
                node.Arguments = append(node.Arguments, arg)
            } else {
                p.newError(tok, "expected type")
            }  
        } else {
            p.newError(tok, "expected word")
        }
        if expectValue(tok, ")") {
            break
        }
    }
    if !expectValue(tok, ")") {
        p.newError(tok, "expected )")
    }
    tok = tok.Next()
    if !expectValue(tok, ":") {
        p.newError(tok, "expected type")
    }
    node.ReturnType, tok = p.parseType(tok)
    // parse the body of the lambda
    if !expectValue(tok, ")") {
        node.Body, tok = p.parseToken(tok)
        for n := node.Body; n != nil && !expectType(tok, TokenEOF) && tok.Value != ")"; n = n.GetNext() {
            var tmp ParserNode
            tmp, tok = p.parseToken(tok)
            n.setNext(tmp)
            if expectValue(tok, ")") {
                break
            }
        }
    }
    if !expectValue(tok, ")") {
        p.newError(tok, "expected closing parenthesis")
    }
    tok = tok.Next()
    return node, tok
}

func (p *parser) parseForm(tok *Token) []*Token {
    tmp := make([]*Token, 0, 3)     
    for t := tok; !expectType(t, TokenEOF); t = t.Next() {
        if expectValue(t, "(") {
            tmp.pattern = append(tmp.pattern, p.parseForm(t))
        } else if expectValue(t, ")") {
            break
        } else {
            
        }
    }
    return tmp
}

func (p *parser) parseMacroPattern(tok *Token) []*form {
    f := make([]*form, 0, 3)
    // start with the macro name
    name := &Token{Value: tok.Value, TokType: tok.TokType}
    f= append(f, &form{pattern: []*Token{name}, ellipsis: false})
    tok = tok.Next()
    tmp := &form{pattern: make([]*Token, 0, 3)}
    
    inside := true
    for t := tok; !expectType(t, TokenEOF); t = t.Next() {
        end := false
        tmp.pattern = append(tmp.pattern, t)
        if inside {
            if expectValue(t, ")") {
                end = true                                
            }
        } else {
            if expectValue(t, "(") {
                inside = true
            } else if expectType(t, TokenWord) {
                end = true
            } else {
                p.newError(t, "expected word or paren")
            }
        }
        if end {
            if expectValue(t.Next(), "...") {
                tmp.ellipsis = true
                t = t.Next()
            }
            f = append(f, tmp)
            tmp = &form{pattern: make([]*Token, 0, 3)}      
        }
    } 
    return f
}

func (p *parser) parseMacro(tok *Token) (*Token) {
    // current token is 'macro'
    tok = tok.Next()
    if !expectValue(tok, "(") {
        p.newError(tok, "expected (")
    }
    tok = tok.Next()
    if expectType(tok, TokenWord) {
        m := &macro{name: tok.Value, pattern: make([]*Token, 0, 3)}
        // read the pattern
        openParens := 1 // keep track of parens
        ellipsis := false
        f := &form{pattern: make([]*Token, 0, 3), ellipsis: false}        
        for ; !expectType(tok, TokenEOF) && openParens != 0; {
            tok = tok.Next()
            if expectValue(tok, ")") {
                openParens--
            } else if expectValue(tok, "(") {
                openParens++
            } else if !expectType(tok, TokenOther) && !expectType(tok, TokenEOF) {
                if ellipsis {
                    p.newError(tok, "expected end of pattern after ellipsis")
                }
                // get parameter names
                // used to replace names in macro
                // with argument values
                if expectValue(tok.Next(), "...") {
                    if ellipsis {
                        p.newError(tok.Next(), "can't have multiple ellipsis")
                    }
                    ellipsis = true
                    f.ellipsis = true
                }
                m.patternParams = append(m.patternParams, 
                                    &patternParam{name: tok.Value, ellipsis: ellipsis})
            }
            if openParens != 0 && !expectValue(tok, "...") { 
                // we want to skip the ellipsis
                // because it's a special character
                m.pattern = append(m.pattern, tok)
            }                        
        }
        if !expectValue(tok, ")") {
            p.newError(tok, "expected )")
        }
        tok = tok.Next()
        m.result, tok = p.parseToken(tok)
        p.macros = append(p.macros, m)        
    } else {
        p.newError(tok, "multiple patterns aren't supported")
    }
    if !expectValue(tok, ")") {
        p.newError(tok, "macro forms only allow one expression")
    }
    tok = tok.Next()
    return tok
}

func (p *parser) parseFunctionCall(tok *Token) (ParserNode, *Token) {
    node := &FunctionCallNode{}
    var macroTmp *macro
    if expectType(tok, TokenWord) {
        for _, m := range p.macros {
            if tok.Value == m.name {
                node.isMacro = true
                macroTmp = m
            }
        }
    }
    node.Function, tok = p.parseToken(tok)
    if !expectValue(tok, ")") {
        if node.isMacro {
            n := node.Arguments
            for i := 0; i < len(macroTmp.pattern); i++ {
                t := macroTmp.pattern[i]
                if expectType(t, TokenOther) {
                    if tok.Value != t.Value {
                        // TokenOther must have same value
                        p.newError(tok, "not valid pattern")
                    } else {
                        // skip the other token
                        // so we don't think it's a func call
                        tok = tok.Next()
                        i++
                        if i < len(macroTmp.pattern) {
                            t = macroTmp.pattern[i]
                        } else {
                            break
                        }
                    }
                }
                var tmp ParserNode
                tmp, tok = p.parseToken(tok)
                if node.Arguments == nil {
                    node.Arguments = tmp
                    n = node.Arguments
                } else {
                    n.setNext(tmp)
                    n = n.GetNext()                    
                }
                if expectType(tok, TokenEOF) {
                    p.newError(tok, "unexpected end of file")
                    break
                }
            }
        } else {
            node.Arguments, tok = p.parseToken(tok)
            for n := node.Arguments; n != nil && !expectType(tok, TokenEOF); n = n.GetNext() {
                var tmp ParserNode          
                tmp, tok = p.parseToken(tok)
                n.setNext(tmp)
                if expectValue(tok, ")") {
                    break
                }
            }
        }
    }
    if !expectValue(tok, ")") {
        p.newError(tok, "expected closing parenthesis")
    }
    tok = tok.Next()
    return node, tok
}

func (p *parser) parseStruct(tok *Token) (ParserNode, *Token) {
    node := &StructNode{}
    tok = tok.Next()
    if !expectType(tok, TokenWord) {
        p.newError(tok, "expected identifier")
    }
    node.Name = tok.Value
    tok = tok.Next()
    if !expectValue(tok, "(") {
        p.newError(tok, "expected (")
    }
    tok = tok.Next()
    for ; tok != nil && tok.Value != ")"; {
        if expectType(tok, TokenWord) { // make sure it's a word
            arg := argument{}
            arg.name = tok.Value     
            tok = tok.Next()                
            if expectValue(tok, ":") { // make sure it's the start of a type
                arg.argType, tok = p.parseType(tok)
                node.Fields = append(node.Fields, arg)
            } else {
                p.newError(tok, "expected type")
            }  
        } else {
            p.newError(tok, "expected word")
        }
        if expectValue(tok, ")") {
            break
        }
    }
    if !expectValue(tok, ")") {
        p.newError(tok, "expected )")
    }
    tok = tok.Next()
    if !expectValue(tok, ")") {
        p.newError(tok, "expected closing parenthesis")
    }
    tok = tok.Next()
    return node, tok
}

func (p *parser) parseExpr(tok *Token) (ParserNode, *Token) {
    var node ParserNode
    switch tok.Value {
    case "var":
        node, tok = p.parseDefVar(tok)
    case "lambda":
        node, tok = p.parseLambda(tok)
    case "macro":
        tok = p.parseMacro(tok)
        node, tok = p.parseToken(tok)
    case "if":
        ifNode := &IfNode{}
        tok = tok.Next()
        ifNode.BooleanExpr, tok = p.parseToken(tok)
        ifNode.TrueExpr, tok = p.parseToken(tok)
        ifNode.FalseExpr, tok = p.parseToken(tok)
        node = ifNode
        tok = tok.Next()
    case "struct":
        node, tok = p.parseStruct(tok)
    default:
        node, tok = p.parseFunctionCall(tok)    
        //p.newError(tok, "unknown function")    
    }
    
    return node, tok
}

func (p *parser) parseToken(tok *Token) (ParserNode, *Token) {
    var node ParserNode
    if expectType(tok, TokenOther) {
        switch tok.Value {
        case "(":
            node, tok = p.parseExpr(tok.Next())
        case ":":
            node, tok = p.parseType(tok)
        default:
            p.newError(tok, "unknown token %s", tok.Value)
            node = &ErrorNode{}
            tok = tok.Next()
        }
    } else {
        switch true {
        case expectType(tok, TokenString) || expectType(tok, TokenNumber) || expectType(tok, TokenWord):
            node = &LiteralNode{Value: tok.Value, LiteralType: tok.TokType}
            tok = tok.Next()
        }
    }
    return node, tok
}

func Parse(tok *Token) ParserNode {
    if tok == nil {
        return nil
    }
    p := &parser{ errs: make([]error, 0, 3) }
    node, tok := p.parseToken(tok)
    for ; !expectType(tok, TokenEOF); node, tok = p.parseToken(tok) {
        node.setNext(node)
    }
    for i := 0; i < len(p.errs); i++ {
        fmt.Print(p.errs[i])
    }
    return node
}