package iliad

const (
    VAR_CUSTOM=iota
    VAR_STRING=iota
    VAR_INTNUM=iota
    VAR_DEFAULT=iota
)
const(
    TYPE_NAME_INTNUM="int"
    TYPE_NAME_STRING="string"
)
type TypeDef interface {
    GetType() int
    GetName() string
    GetDefault() interface{}
}

type Default struct {
    name string
}


type Custom struct {
    name string
    // TODO(darithorn): figure out how to express default value
}
func CreateCustom(name string) *Custom {
    return &Custom{name:name}
}
func (c *Custom) GetType() int {
    return VAR_CUSTOM
}
func (c *Custom) GetName() string {
    return c.name
}

type IntNum struct {
    DefValue int // the default value
}
func CreateIntNum() *IntNum {
    return &IntNum{ DefValue: 0 }
}
func (i *IntNum) GetType() int {
    return VAR_INTNUM
}
func (i *IntNum) GetName() string {
    return TYPE_NAME_INTNUM
}

type String struct {
    DefValue string // the default value
}
func CreateString() *String {
    return &String{ DefValue: "" }
}
func (s *String) GetType() int {
    return VAR_STRING
}
func (s *String) GetName() string {
    return TYPE_NAME_STRING
}