Types
=====
:string
:char
:int
:bool
:void

Arrays
======
allows consecutive []
:[]string

Pointers
========
:*{name}

Typedeff'd
===========
:{name}

Function Type
=============
:(func ([arg_type ...]) {return_type})

Macro Type
==========
:(macro ([pattern ...])

Allowed Names
=============
any letter
><-/\=?!*

Comments
========
; This is a comment

Structures
==========
creating
({name} {value ...})
accessing
({name}-{field} {variable_name})
setting
(set-{name}-{field}! {variable_name} {value})

(ref {variable}) // & as sugar in the future?
(import {name ...})
(var {name} {type})
(var {name} {value})
(set! {name} {value})
(if {condition} {true} {false})
(lambda ([arg type ...]) {return_type} {body ...})
(func ({name} [arg type ...]) {return_type} {body ...}) // Could be a macro?
(struct {name} ([field type ...])) // Built-in to allow compiler optimization
(cast {type} {value})

Example
=======
(import "io")
(func main (args :[]string) :int
    (var foo "hello")
    (var bar :string)
    (set! bar "world")
    (printf foo)
    0)